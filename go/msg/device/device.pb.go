// Code generated by protoc-gen-go. DO NOT EDIT.
// source: messages/device.proto

package device // import "bitbucket.org/thinggate/tg-proto/go/msg/device"

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import basetypes "bitbucket.org/thinggate/tg-proto/go/msg/basetypes"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Box struct {
	Devices              []*Device `protobuf:"bytes,1,rep,name=Devices" json:"Devices"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *Box) Reset()         { *m = Box{} }
func (m *Box) String() string { return proto.CompactTextString(m) }
func (*Box) ProtoMessage()    {}
func (*Box) Descriptor() ([]byte, []int) {
	return fileDescriptor_device_2d42191955fbdba2, []int{0}
}
func (m *Box) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Box.Unmarshal(m, b)
}
func (m *Box) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Box.Marshal(b, m, deterministic)
}
func (dst *Box) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Box.Merge(dst, src)
}
func (m *Box) XXX_Size() int {
	return xxx_messageInfo_Box.Size(m)
}
func (m *Box) XXX_DiscardUnknown() {
	xxx_messageInfo_Box.DiscardUnknown(m)
}

var xxx_messageInfo_Box proto.InternalMessageInfo

func (m *Box) GetDevices() []*Device {
	if m != nil {
		return m.Devices
	}
	return nil
}

type BoxUpdate struct {
	Identifier           string               `protobuf:"bytes,1,opt,name=Identifier" json:"Identifier"`
	Box                  *Box                 `protobuf:"bytes,2,opt,name=Box" json:"Box"`
	Type                 basetypes.UpdateType `protobuf:"varint,3,opt,name=Type,enum=UpdateType" json:"Type"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *BoxUpdate) Reset()         { *m = BoxUpdate{} }
func (m *BoxUpdate) String() string { return proto.CompactTextString(m) }
func (*BoxUpdate) ProtoMessage()    {}
func (*BoxUpdate) Descriptor() ([]byte, []int) {
	return fileDescriptor_device_2d42191955fbdba2, []int{1}
}
func (m *BoxUpdate) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BoxUpdate.Unmarshal(m, b)
}
func (m *BoxUpdate) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BoxUpdate.Marshal(b, m, deterministic)
}
func (dst *BoxUpdate) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BoxUpdate.Merge(dst, src)
}
func (m *BoxUpdate) XXX_Size() int {
	return xxx_messageInfo_BoxUpdate.Size(m)
}
func (m *BoxUpdate) XXX_DiscardUnknown() {
	xxx_messageInfo_BoxUpdate.DiscardUnknown(m)
}

var xxx_messageInfo_BoxUpdate proto.InternalMessageInfo

func (m *BoxUpdate) GetIdentifier() string {
	if m != nil {
		return m.Identifier
	}
	return ""
}

func (m *BoxUpdate) GetBox() *Box {
	if m != nil {
		return m.Box
	}
	return nil
}

func (m *BoxUpdate) GetType() basetypes.UpdateType {
	if m != nil {
		return m.Type
	}
	return basetypes.UpdateType_NEW
}

type Device struct {
	Identifier           string   `protobuf:"bytes,1,opt,name=Identifier" json:"Identifier"`
	BoxIdentifier        string   `protobuf:"bytes,2,opt,name=BoxIdentifier" json:"BoxIdentifier"`
	Name                 string   `protobuf:"bytes,3,opt,name=Name" json:"Name"`
	Items                []*Item  `protobuf:"bytes,4,rep,name=Items" json:"Items"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Device) Reset()         { *m = Device{} }
func (m *Device) String() string { return proto.CompactTextString(m) }
func (*Device) ProtoMessage()    {}
func (*Device) Descriptor() ([]byte, []int) {
	return fileDescriptor_device_2d42191955fbdba2, []int{2}
}
func (m *Device) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Device.Unmarshal(m, b)
}
func (m *Device) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Device.Marshal(b, m, deterministic)
}
func (dst *Device) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Device.Merge(dst, src)
}
func (m *Device) XXX_Size() int {
	return xxx_messageInfo_Device.Size(m)
}
func (m *Device) XXX_DiscardUnknown() {
	xxx_messageInfo_Device.DiscardUnknown(m)
}

var xxx_messageInfo_Device proto.InternalMessageInfo

func (m *Device) GetIdentifier() string {
	if m != nil {
		return m.Identifier
	}
	return ""
}

func (m *Device) GetBoxIdentifier() string {
	if m != nil {
		return m.BoxIdentifier
	}
	return ""
}

func (m *Device) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Device) GetItems() []*Item {
	if m != nil {
		return m.Items
	}
	return nil
}

type DeviceUpdate struct {
	Device               *Device              `protobuf:"bytes,1,opt,name=Device" json:"Device"`
	Type                 basetypes.UpdateType `protobuf:"varint,2,opt,name=Type,enum=UpdateType" json:"Type"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *DeviceUpdate) Reset()         { *m = DeviceUpdate{} }
func (m *DeviceUpdate) String() string { return proto.CompactTextString(m) }
func (*DeviceUpdate) ProtoMessage()    {}
func (*DeviceUpdate) Descriptor() ([]byte, []int) {
	return fileDescriptor_device_2d42191955fbdba2, []int{3}
}
func (m *DeviceUpdate) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeviceUpdate.Unmarshal(m, b)
}
func (m *DeviceUpdate) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeviceUpdate.Marshal(b, m, deterministic)
}
func (dst *DeviceUpdate) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeviceUpdate.Merge(dst, src)
}
func (m *DeviceUpdate) XXX_Size() int {
	return xxx_messageInfo_DeviceUpdate.Size(m)
}
func (m *DeviceUpdate) XXX_DiscardUnknown() {
	xxx_messageInfo_DeviceUpdate.DiscardUnknown(m)
}

var xxx_messageInfo_DeviceUpdate proto.InternalMessageInfo

func (m *DeviceUpdate) GetDevice() *Device {
	if m != nil {
		return m.Device
	}
	return nil
}

func (m *DeviceUpdate) GetType() basetypes.UpdateType {
	if m != nil {
		return m.Type
	}
	return basetypes.UpdateType_NEW
}

type Item struct {
	Identifier           string      `protobuf:"bytes,1,opt,name=Identifier" json:"Identifier"`
	BoxIdentifier        string      `protobuf:"bytes,2,opt,name=BoxIdentifier" json:"BoxIdentifier"`
	DeviceIdentifier     string      `protobuf:"bytes,3,opt,name=DeviceIdentifier" json:"DeviceIdentifier"`
	Name                 string      `protobuf:"bytes,4,opt,name=Name" json:"Name"`
	Type                 string      `protobuf:"bytes,5,opt,name=Type" json:"Type"`
	Properties           []*Property `protobuf:"bytes,10,rep,name=Properties" json:"Properties"`
	XXX_NoUnkeyedLiteral struct{}    `json:"-"`
	XXX_unrecognized     []byte      `json:"-"`
	XXX_sizecache        int32       `json:"-"`
}

func (m *Item) Reset()         { *m = Item{} }
func (m *Item) String() string { return proto.CompactTextString(m) }
func (*Item) ProtoMessage()    {}
func (*Item) Descriptor() ([]byte, []int) {
	return fileDescriptor_device_2d42191955fbdba2, []int{4}
}
func (m *Item) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Item.Unmarshal(m, b)
}
func (m *Item) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Item.Marshal(b, m, deterministic)
}
func (dst *Item) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Item.Merge(dst, src)
}
func (m *Item) XXX_Size() int {
	return xxx_messageInfo_Item.Size(m)
}
func (m *Item) XXX_DiscardUnknown() {
	xxx_messageInfo_Item.DiscardUnknown(m)
}

var xxx_messageInfo_Item proto.InternalMessageInfo

func (m *Item) GetIdentifier() string {
	if m != nil {
		return m.Identifier
	}
	return ""
}

func (m *Item) GetBoxIdentifier() string {
	if m != nil {
		return m.BoxIdentifier
	}
	return ""
}

func (m *Item) GetDeviceIdentifier() string {
	if m != nil {
		return m.DeviceIdentifier
	}
	return ""
}

func (m *Item) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Item) GetType() string {
	if m != nil {
		return m.Type
	}
	return ""
}

func (m *Item) GetProperties() []*Property {
	if m != nil {
		return m.Properties
	}
	return nil
}

type Property struct {
	Identifier       string `protobuf:"bytes,1,opt,name=Identifier" json:"Identifier"`
	BoxIdentifier    string `protobuf:"bytes,2,opt,name=BoxIdentifier" json:"BoxIdentifier"`
	DeviceIdentifier string `protobuf:"bytes,3,opt,name=DeviceIdentifier" json:"DeviceIdentifier"`
	ItemIdentifier   string `protobuf:"bytes,4,opt,name=ItemIdentifier" json:"ItemIdentifier"`
	Name             string `protobuf:"bytes,5,opt,name=Name" json:"Name"`
	// Types that are valid to be assigned to Value:
	//	*Property_Boolean
	//	*Property_Number
	//	*Property_Decimal
	//	*Property_Text
	//	*Property_List
	Value                isProperty_Value `protobuf_oneof:"Value"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

func (m *Property) Reset()         { *m = Property{} }
func (m *Property) String() string { return proto.CompactTextString(m) }
func (*Property) ProtoMessage()    {}
func (*Property) Descriptor() ([]byte, []int) {
	return fileDescriptor_device_2d42191955fbdba2, []int{5}
}
func (m *Property) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Property.Unmarshal(m, b)
}
func (m *Property) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Property.Marshal(b, m, deterministic)
}
func (dst *Property) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Property.Merge(dst, src)
}
func (m *Property) XXX_Size() int {
	return xxx_messageInfo_Property.Size(m)
}
func (m *Property) XXX_DiscardUnknown() {
	xxx_messageInfo_Property.DiscardUnknown(m)
}

var xxx_messageInfo_Property proto.InternalMessageInfo

type isProperty_Value interface {
	isProperty_Value()
}

type Property_Boolean struct {
	Boolean bool `protobuf:"varint,10,opt,name=Boolean,oneof"`
}
type Property_Number struct {
	Number int64 `protobuf:"varint,11,opt,name=Number,oneof"`
}
type Property_Decimal struct {
	Decimal float32 `protobuf:"fixed32,12,opt,name=Decimal,oneof"`
}
type Property_Text struct {
	Text string `protobuf:"bytes,13,opt,name=Text,oneof"`
}
type Property_List struct {
	List *basetypes.ValueList `protobuf:"bytes,14,opt,name=List,oneof"`
}

func (*Property_Boolean) isProperty_Value() {}
func (*Property_Number) isProperty_Value()  {}
func (*Property_Decimal) isProperty_Value() {}
func (*Property_Text) isProperty_Value()    {}
func (*Property_List) isProperty_Value()    {}

func (m *Property) GetValue() isProperty_Value {
	if m != nil {
		return m.Value
	}
	return nil
}

func (m *Property) GetIdentifier() string {
	if m != nil {
		return m.Identifier
	}
	return ""
}

func (m *Property) GetBoxIdentifier() string {
	if m != nil {
		return m.BoxIdentifier
	}
	return ""
}

func (m *Property) GetDeviceIdentifier() string {
	if m != nil {
		return m.DeviceIdentifier
	}
	return ""
}

func (m *Property) GetItemIdentifier() string {
	if m != nil {
		return m.ItemIdentifier
	}
	return ""
}

func (m *Property) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Property) GetBoolean() bool {
	if x, ok := m.GetValue().(*Property_Boolean); ok {
		return x.Boolean
	}
	return false
}

func (m *Property) GetNumber() int64 {
	if x, ok := m.GetValue().(*Property_Number); ok {
		return x.Number
	}
	return 0
}

func (m *Property) GetDecimal() float32 {
	if x, ok := m.GetValue().(*Property_Decimal); ok {
		return x.Decimal
	}
	return 0
}

func (m *Property) GetText() string {
	if x, ok := m.GetValue().(*Property_Text); ok {
		return x.Text
	}
	return ""
}

func (m *Property) GetList() *basetypes.ValueList {
	if x, ok := m.GetValue().(*Property_List); ok {
		return x.List
	}
	return nil
}

// XXX_OneofFuncs is for the internal use of the proto package.
func (*Property) XXX_OneofFuncs() (func(msg proto.Message, b *proto.Buffer) error, func(msg proto.Message, tag, wire int, b *proto.Buffer) (bool, error), func(msg proto.Message) (n int), []interface{}) {
	return _Property_OneofMarshaler, _Property_OneofUnmarshaler, _Property_OneofSizer, []interface{}{
		(*Property_Boolean)(nil),
		(*Property_Number)(nil),
		(*Property_Decimal)(nil),
		(*Property_Text)(nil),
		(*Property_List)(nil),
	}
}

func _Property_OneofMarshaler(msg proto.Message, b *proto.Buffer) error {
	m := msg.(*Property)
	// Value
	switch x := m.Value.(type) {
	case *Property_Boolean:
		t := uint64(0)
		if x.Boolean {
			t = 1
		}
		b.EncodeVarint(10<<3 | proto.WireVarint)
		b.EncodeVarint(t)
	case *Property_Number:
		b.EncodeVarint(11<<3 | proto.WireVarint)
		b.EncodeVarint(uint64(x.Number))
	case *Property_Decimal:
		b.EncodeVarint(12<<3 | proto.WireFixed32)
		b.EncodeFixed32(uint64(math.Float32bits(x.Decimal)))
	case *Property_Text:
		b.EncodeVarint(13<<3 | proto.WireBytes)
		b.EncodeStringBytes(x.Text)
	case *Property_List:
		b.EncodeVarint(14<<3 | proto.WireBytes)
		if err := b.EncodeMessage(x.List); err != nil {
			return err
		}
	case nil:
	default:
		return fmt.Errorf("Property.Value has unexpected type %T", x)
	}
	return nil
}

func _Property_OneofUnmarshaler(msg proto.Message, tag, wire int, b *proto.Buffer) (bool, error) {
	m := msg.(*Property)
	switch tag {
	case 10: // Value.Boolean
		if wire != proto.WireVarint {
			return true, proto.ErrInternalBadWireType
		}
		x, err := b.DecodeVarint()
		m.Value = &Property_Boolean{x != 0}
		return true, err
	case 11: // Value.Number
		if wire != proto.WireVarint {
			return true, proto.ErrInternalBadWireType
		}
		x, err := b.DecodeVarint()
		m.Value = &Property_Number{int64(x)}
		return true, err
	case 12: // Value.Decimal
		if wire != proto.WireFixed32 {
			return true, proto.ErrInternalBadWireType
		}
		x, err := b.DecodeFixed32()
		m.Value = &Property_Decimal{math.Float32frombits(uint32(x))}
		return true, err
	case 13: // Value.Text
		if wire != proto.WireBytes {
			return true, proto.ErrInternalBadWireType
		}
		x, err := b.DecodeStringBytes()
		m.Value = &Property_Text{x}
		return true, err
	case 14: // Value.List
		if wire != proto.WireBytes {
			return true, proto.ErrInternalBadWireType
		}
		msg := new(basetypes.ValueList)
		err := b.DecodeMessage(msg)
		m.Value = &Property_List{msg}
		return true, err
	default:
		return false, nil
	}
}

func _Property_OneofSizer(msg proto.Message) (n int) {
	m := msg.(*Property)
	// Value
	switch x := m.Value.(type) {
	case *Property_Boolean:
		n += 1 // tag and wire
		n += 1
	case *Property_Number:
		n += 1 // tag and wire
		n += proto.SizeVarint(uint64(x.Number))
	case *Property_Decimal:
		n += 1 // tag and wire
		n += 4
	case *Property_Text:
		n += 1 // tag and wire
		n += proto.SizeVarint(uint64(len(x.Text)))
		n += len(x.Text)
	case *Property_List:
		s := proto.Size(x.List)
		n += 1 // tag and wire
		n += proto.SizeVarint(uint64(s))
		n += s
	case nil:
	default:
		panic(fmt.Sprintf("proto: unexpected type %T in oneof", x))
	}
	return n
}

type PropertyUpdate struct {
	State                *Property            `protobuf:"bytes,1,opt,name=State" json:"State"`
	Type                 basetypes.UpdateType `protobuf:"varint,2,opt,name=Type,enum=UpdateType" json:"Type"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *PropertyUpdate) Reset()         { *m = PropertyUpdate{} }
func (m *PropertyUpdate) String() string { return proto.CompactTextString(m) }
func (*PropertyUpdate) ProtoMessage()    {}
func (*PropertyUpdate) Descriptor() ([]byte, []int) {
	return fileDescriptor_device_2d42191955fbdba2, []int{6}
}
func (m *PropertyUpdate) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PropertyUpdate.Unmarshal(m, b)
}
func (m *PropertyUpdate) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PropertyUpdate.Marshal(b, m, deterministic)
}
func (dst *PropertyUpdate) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PropertyUpdate.Merge(dst, src)
}
func (m *PropertyUpdate) XXX_Size() int {
	return xxx_messageInfo_PropertyUpdate.Size(m)
}
func (m *PropertyUpdate) XXX_DiscardUnknown() {
	xxx_messageInfo_PropertyUpdate.DiscardUnknown(m)
}

var xxx_messageInfo_PropertyUpdate proto.InternalMessageInfo

func (m *PropertyUpdate) GetState() *Property {
	if m != nil {
		return m.State
	}
	return nil
}

func (m *PropertyUpdate) GetType() basetypes.UpdateType {
	if m != nil {
		return m.Type
	}
	return basetypes.UpdateType_NEW
}

func init() {
	proto.RegisterType((*Box)(nil), "Box")
	proto.RegisterType((*BoxUpdate)(nil), "BoxUpdate")
	proto.RegisterType((*Device)(nil), "Device")
	proto.RegisterType((*DeviceUpdate)(nil), "DeviceUpdate")
	proto.RegisterType((*Item)(nil), "Item")
	proto.RegisterType((*Property)(nil), "Property")
	proto.RegisterType((*PropertyUpdate)(nil), "PropertyUpdate")
}

func init() { proto.RegisterFile("messages/device.proto", fileDescriptor_device_2d42191955fbdba2) }

var fileDescriptor_device_2d42191955fbdba2 = []byte{
	// 496 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xc4, 0x94, 0x51, 0x6b, 0xdb, 0x30,
	0x10, 0xc7, 0xe3, 0xc4, 0x4e, 0x9a, 0x4b, 0x1b, 0x86, 0xd8, 0x86, 0x68, 0x61, 0xf5, 0xcc, 0x18,
	0xde, 0x60, 0x0a, 0x64, 0xdf, 0xc0, 0xf4, 0x21, 0x85, 0x51, 0x8a, 0xd6, 0xf5, 0x61, 0x6f, 0x4a,
	0x72, 0xf3, 0xc4, 0x62, 0xcb, 0x58, 0xca, 0x70, 0x9e, 0xf6, 0x01, 0xfb, 0xa5, 0x86, 0x24, 0x3b,
	0x78, 0x1b, 0xa3, 0x2f, 0x83, 0x3d, 0x45, 0xf7, 0xbb, 0xd3, 0xdd, 0xfd, 0x2f, 0x67, 0xc1, 0xb3,
	0x02, 0xb5, 0x16, 0x39, 0xea, 0xc5, 0x16, 0xbf, 0xcb, 0x0d, 0xb2, 0xaa, 0x56, 0x46, 0x9d, 0xd3,
	0x23, 0x5e, 0x0b, 0x8d, 0xe6, 0x50, 0xa1, 0xf6, 0x9e, 0x24, 0x85, 0x51, 0xa6, 0x1a, 0xf2, 0x12,
	0x26, 0x57, 0xee, 0x82, 0xa6, 0x41, 0x3c, 0x4a, 0x67, 0xcb, 0x09, 0xf3, 0x36, 0xef, 0x78, 0xb2,
	0x85, 0x69, 0xa6, 0x9a, 0x4f, 0xd5, 0x56, 0x18, 0x24, 0x2f, 0x00, 0xae, 0xb7, 0x58, 0x1a, 0xf9,
	0x45, 0x62, 0x4d, 0x83, 0x38, 0x48, 0xa7, 0xbc, 0x47, 0xc8, 0x73, 0x97, 0x96, 0x0e, 0xe3, 0x20,
	0x9d, 0x2d, 0x43, 0x96, 0xa9, 0x86, 0xbb, 0x3a, 0x97, 0x10, 0xde, 0x1d, 0x2a, 0xa4, 0xa3, 0x38,
	0x48, 0xe7, 0xcb, 0x19, 0xf3, 0xe9, 0x2c, 0xe2, 0xce, 0x91, 0xfc, 0x80, 0xb1, 0x2f, 0xf8, 0x68,
	0x89, 0x57, 0x70, 0x96, 0xa9, 0xa6, 0x17, 0x32, 0x74, 0x21, 0xbf, 0x42, 0x42, 0x20, 0xbc, 0x11,
	0x85, 0x2f, 0x38, 0xe5, 0xee, 0x4c, 0x2e, 0x20, 0xba, 0x36, 0x58, 0x68, 0x1a, 0x3a, 0xa9, 0x11,
	0xb3, 0x16, 0xf7, 0x2c, 0xb9, 0x85, 0x53, 0xdf, 0x40, 0xab, 0xf4, 0xb2, 0x6b, 0xc8, 0xb5, 0xd0,
	0x1b, 0x4c, 0xd7, 0x67, 0x27, 0x69, 0xf8, 0x37, 0x49, 0x0f, 0x01, 0x84, 0x36, 0xf7, 0x3f, 0x52,
	0xf4, 0x16, 0x9e, 0xf8, 0xca, 0xbd, 0x40, 0xaf, 0xee, 0x0f, 0x7e, 0x54, 0x1f, 0xf6, 0xd4, 0x93,
	0xb6, 0xdf, 0xc8, 0x33, 0x7b, 0x26, 0x6f, 0x00, 0x6e, 0x6b, 0x55, 0x61, 0x6d, 0x24, 0x6a, 0x0a,
	0x6e, 0x2c, 0x53, 0xd6, 0xa2, 0x03, 0xef, 0x39, 0x93, 0x87, 0x21, 0x9c, 0x74, 0x8e, 0xff, 0xa0,
	0xe8, 0x35, 0xcc, 0xed, 0x2c, 0x7b, 0x91, 0x5e, 0xdb, 0x6f, 0xf4, 0xa8, 0x3c, 0xea, 0x29, 0x3f,
	0x87, 0x49, 0xa6, 0xd4, 0x0e, 0x45, 0x49, 0x21, 0x0e, 0xd2, 0x93, 0xd5, 0x80, 0x77, 0x80, 0x50,
	0x18, 0xdf, 0xec, 0x8b, 0x35, 0xd6, 0x74, 0x16, 0x07, 0xe9, 0x68, 0x35, 0xe0, 0xad, 0x6d, 0x6f,
	0x5d, 0xe1, 0x46, 0x16, 0x62, 0x47, 0x4f, 0xe3, 0x20, 0x1d, 0xda, 0x5b, 0x2d, 0x20, 0x4f, 0x21,
	0xbc, 0xc3, 0xc6, 0xd0, 0x33, 0x5b, 0x65, 0x35, 0xe0, 0xce, 0x22, 0x31, 0x84, 0x1f, 0xa4, 0x36,
	0x74, 0xee, 0x16, 0x06, 0xd8, 0xbd, 0xd8, 0xed, 0xd1, 0x12, 0x1b, 0x61, 0x7f, 0xb3, 0x09, 0x44,
	0x0e, 0x26, 0x1c, 0xe6, 0xdd, 0x30, 0x8f, 0xfb, 0x16, 0x7d, 0x34, 0xc2, 0x74, 0xeb, 0xd6, 0xfb,
	0x17, 0x3c, 0x7f, 0x74, 0xdf, 0xb2, 0x7b, 0xb8, 0xd8, 0xa8, 0x82, 0x35, 0x12, 0x1b, 0x89, 0xac,
	0x56, 0x85, 0x28, 0x99, 0xd8, 0x6f, 0xa5, 0xaa, 0x44, 0x89, 0xbb, 0xac, 0xfb, 0xa0, 0x3f, 0xb3,
	0xb5, 0x34, 0xeb, 0xfd, 0xe6, 0x1b, 0x1a, 0xa6, 0xea, 0x7c, 0x61, 0xbe, 0xca, 0x32, 0xcf, 0x85,
	0xc1, 0x85, 0xc9, 0xdf, 0xb9, 0xc7, 0x61, 0x91, 0xab, 0x45, 0xa1, 0xf3, 0xf6, 0x29, 0x59, 0x8f,
	0x1d, 0x7c, 0xff, 0x33, 0x00, 0x00, 0xff, 0xff, 0x04, 0x64, 0x45, 0xc1, 0x64, 0x04, 0x00, 0x00,
}
